# Struts 2 Hello World

1. Instalar Maven
1. Crear proyecto con arquetipo struts. Elegir uno de los arquetipos desde https://struts.apache.org/docs/struts-2-maven-archetypes.html. Se recomienda el arquetipo "struts2-archetype-blank". 
Ejecutar entonces (reemplazar <CURRENT_STRUTS_VERSION> por "2.5":

```
mvn archetype:generate -B -DgroupId=com.mycompany.mysystem \
                            -DartifactId=myWebApp \
                            -DarchetypeGroupId=org.apache.struts \
                            -DarchetypeArtifactId=struts2-archetype-blank \
                            -DarchetypeVersion=2.5 \
                            -DremoteRepositories=http://struts.apache.org
```

1. Para probar que está todo OK, entrar en la nueva carpeta generada y ejecutar: `mvn jetty:run`, esto debiera, luego de descargar más dependencias, ejecutar la aplicación en `localhost:8080`. 

1. Para generar un archivo de proyecto para Eclipse o IntellIJ ejecutar: `mvn eclipse:eclipse -Dwtpversion=1.5` o bien `mvn idea:idea`.

1. Struts puede considerarse un framework MVC, pero la terminología que ocupa es diferente:
    1. **Rutas**: las rutas mapean patrones de URL con la ejecución de respuestas tales como: ejecutar una _Acción_, redireccionar, etc. Se configuran en el archivo `struts.xml`, el cual puede incluir otros XML para manejar aplicaciones muy grandes. Las respuestas pueden discriminar el resultado de la acción, es decir si fue exitosa o no.

   1. **Vistas**: son archivos JSP que están bajo la carpeta `webapp`. Se pueden especificar como respuesta a los resultados de una acción.

   1. **Acciones**: son clases Java que extienden la clase `Action`. El arquetipo generado más arriba utiliza la clase `ActionSupport` que es mucho más conveniente. Una acción tiene un método `execute()` que se invoca cuando la acción se ejecuta como respuesta a una ruta solicitada por el usuario. La acción puede retornar distintos resultados, entre ellos `"success"` y `"error"` para diferenciar casos exitosos de los que no lo son.

## Sobre los parámetros por URL

Si una acción define un atributo `message` y un setter `setMessage(message)`, Struts automáticamente mapea el parámetro desde la URL e invoca al setter. En cierto modo, el mapeo de URL a acciones hace que se instancie un objeto, con los campos ya seteados según los parámetros ingresados por el usuario.

## Integración con Hibernate

https://www.tutorialspoint.com/struts_2/struts_hibernate.htm
https://www.mkyong.com/struts2/struts-2-hibernate-integration-example/